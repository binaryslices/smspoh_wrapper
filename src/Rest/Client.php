<?php

namespace Escapepixel\Smspoh\Rest;

use Escapepixel\Smspoh\Http\GuzzleClient;

class Client {
    /**
     * Initializes the Client
     */
    public function __construct($token, $endPoint)
    {
        $this->token = $token;
        $this->endPoint = $endPoint;
        $this->client = new GuzzleClient($endPoint, $token);
    }


    /**
     * Send Message to the Specified Number using the configured http client
     */
    public function sendSms($to, $message)
    {
        $response = $this->client->request([
            'to' => $to,
            'message' => $message,
        ]);
        return $response['status'];
    }
}