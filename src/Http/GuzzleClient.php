<?php

namespace Escapepixel\Smspoh\Http;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

class GuzzleClient {
    /**
     * @var ClientInterface
     */
    protected $endpoint;
    protected $token;
    protected $guzzle;
    public function __construct($endpoint, $token)
    {
        $this->endpoint = $endpoint;
        $this->token = $token;
        $this->guzzle = new Client();
    }

    public function request($data)
    {
        try {        
            $endpoint = $this->endpoint;

            $response = $this->guzzle->request(
                'POST', $endpoint,[
                'headers' => [
                    'Authorization' => "Bearer {$this->token}"
                ],
                'json' => [
                    'message' => $data['message'],
                    'to' => $data['to']
                ]
            ]);

            $result = json_decode( $response->getBody()->getContents(),true);

            return $result;
        } catch (ClientException $e) {

            if($e->getCode()===401){
                throw new Exception(
                    $e->getMessage(),
                    401
                );
            } elseif ($e->getCode()===403) {
                throw new Exception(
                    $e->getMessage(),
                    403
                );
            }
        }
    }
}